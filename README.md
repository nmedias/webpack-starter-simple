# Webpack Frontend Starter

[![Dependabot badge](https://flat.badgen.net/dependabot/wbkd/webpack-starter?icon=dependabot)](https://dependabot.com/)

A lightweight foundation for your next webpack based frontend project.


### Install

```
npm install
```

### Developments

```
npm run dev
```

### Build for Production

```
npm run build
```

### Features:

* TypeScript Support (Checker, Compiler) via [awesome-typescript-loader](https://github.com/s-panferov/awesome-typescript-loader)
* ES6 Support via [babel](https://babeljs.io/) (v7)
* SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting (.js, .ts) via [eslint-loader](https://github.com/MoOx/eslint-loader)
* Prettier (.js, .ts)
* AutoPrefixer (postCSS)

When you run `npm run build` it uses the [mini-css-extract-plugin](https://github.com/webpack-contrib/mini-css-extract-plugin) to move the css to a separate file. The css file gets included in the head of the `index.html` via `HtmlWebpackPlugin`.
